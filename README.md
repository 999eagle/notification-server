# notification-server

A Linux notification server written in Rust using GTK. Currently, version 1.2 of the [freedesktop.org notification spec](https://specifications.freedesktop.org/notification-spec/notification-spec-1.2.html) is supported.

## Running

Run from source using `cargo run` or run the compiled binary with `./notification-server`.

## Configuration

The notifications can be styled using GTK CSS. By default, `$XDG_CONFIG_HOME/notification-server/style.css` is read and applied if it exists.

More configuration options may be added in future versions.

## Usage

Notifications will be expired after 5 seconds by default. They can be dismissed earlier by clicking on the notification window.
