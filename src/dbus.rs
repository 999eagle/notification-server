mod notification;

use crate::{
    config::Config,
    messages::{MessageFromDBus, MessageToDBus},
};
use core::convert::TryFrom;
use futures_lite::stream::StreamExt;
use std::sync::Arc;
use tokio::sync::mpsc;
use zbus::names::WellKnownName;
use zbus::Result;

pub type MessageSender = mpsc::Sender<MessageFromDBus>;
pub type MessageReceiver = mpsc::Receiver<MessageToDBus>;
pub use notification::{CloseReason, DBusNotification};

enum DBusLoopEvent {
    ControlMessage(MessageToDBus),
    Nop,
}

pub async fn run_dbus(
    config: Arc<Config>,
    tx: MessageSender,
    mut rx: MessageReceiver,
) -> Result<()> {
    let connection = zbus::ConnectionBuilder::session()?
        .internal_executor(false)
        .build()
        .await?;
    {
        let conn = connection.clone();
        tokio::task::spawn(async move {
            loop {
                conn.executor().tick().await;
            }
        });
    }
    if let Some(name) = connection.unique_name() {
        log::debug!("Connected to dbus at {}", name.as_str());
    }
    let dbus_proxy = zbus::fdo::DBusProxy::new(&connection).await?;
    let mut name_lost_stream = dbus_proxy.receive_name_lost().await?.map(|name| {
        log::info!("Lost name {}", name.args().unwrap().name());
    });
    let mut name_acquired_stream = dbus_proxy.receive_name_acquired().await?.map(|name| {
        log::info!("Acquired name {}", name.args().unwrap().name());
    });
    let result = dbus_proxy
        .request_name(
            WellKnownName::try_from("org.freedesktop.Notifications").unwrap(),
            zbus::fdo::RequestNameFlags::AllowReplacement
                | zbus::fdo::RequestNameFlags::ReplaceExisting,
        )
        .await?;
    match result {
        zbus::fdo::RequestNameReply::PrimaryOwner | zbus::fdo::RequestNameReply::AlreadyOwner => {}
        zbus::fdo::RequestNameReply::Exists | zbus::fdo::RequestNameReply::InQueue => {
            log::debug!("Waiting for notifications name")
        }
    }

    let server_info = notification::ServerInformation {
        name: config.crate_setup.application_name().into(),
        vendor: "999eagle".into(),
        version: version::version!().into(),
        spec_version: "1.2".into(),
    };
    let notification_server = notification::NotificationServer::new(tx, server_info);
    connection
        .object_server()
        .at("/org/freedesktop/Notifications", notification_server)
        .await?;

    loop {
        let result = tokio::select! {
            Some(msg) = rx.recv() => {
                DBusLoopEvent::ControlMessage(msg)
            }
            Some(_) = name_lost_stream.next() => {
                DBusLoopEvent::Nop
            }
            Some(_) = name_acquired_stream.next() => {
                DBusLoopEvent::Nop
            }
        };
        match result {
            DBusLoopEvent::ControlMessage(msg) => match msg {
                MessageToDBus::Exit => {
                    break;
                }
                MessageToDBus::NotificationClosed(id, reason) => {
                    let iface_ref = connection
                        .object_server()
                        .interface::<_, notification::NotificationServer>(
                            "/org/freedesktop/Notifications",
                        )
                        .await?;
                    iface_ref
                        .get()
                        .await
                        .emit_notification_closed(iface_ref.signal_context(), id, reason)
                        .await?;
                }
                MessageToDBus::ActionInvoked(id, action_key) => {
                    let iface_ref = connection
                        .object_server()
                        .interface::<_, notification::NotificationServer>(
                            "/org/freedesktop/Notifications",
                        )
                        .await?;
                    iface_ref
                        .get()
                        .await
                        .emit_action_invoked(iface_ref.signal_context(), id, &action_key)
                        .await?;
                }
            },
            DBusLoopEvent::Nop => {}
        }
    }

    Ok(())
}
