use crate::{
    dbus::CloseReason,
    messages::{MessageFromDBus, MessageFromWindowing, MessageToDBus, MessageToWindowing},
    notification::Notification,
};
use std::{collections::HashMap, sync::Arc, time::Duration};
use tokio::{
    sync::{mpsc, Mutex},
    task, time,
};

struct NotificationData {
    timeout_handle: Option<tokio::task::JoinHandle<()>>,
    duration: Option<Duration>,
    close_reason: Option<CloseReason>,
}

impl NotificationData {
    pub fn new(notification: &Notification) -> Self {
        Self {
            timeout_handle: None,
            duration: notification.duration(),
            close_reason: None,
        }
    }
}

pub async fn run_message_handlers(
    mut from_dbus_rx: mpsc::Receiver<MessageFromDBus>,
    mut from_windowing_rx: mpsc::Receiver<MessageFromWindowing>,
    to_dbus_tx: mpsc::Sender<MessageToDBus>,
    to_windowing_tx: mpsc::Sender<MessageToWindowing>,
) {
    let notification_data = Arc::new(Mutex::new(HashMap::new()));

    let notification_data_cloned = Arc::clone(&notification_data);
    let to_windowing_tx_cloned = to_windowing_tx.clone();
    let from_dbus_handler = task::spawn(crate::util::log_future_result(
        async move {
            while let Some(msg) = from_dbus_rx.recv().await {
                match msg {
                    MessageFromDBus::NotificationReceived(notification)
                        if notification.replaces_id.is_none() =>
                    {
                        {
                            let mut notification_data = notification_data_cloned.lock().await;
                            notification_data
                                .insert(notification.id, NotificationData::new(&notification));
                        }
                        to_windowing_tx_cloned
                            .send(MessageToWindowing::DisplayNotification(notification))
                            .await?;
                    }
                    MessageFromDBus::NotificationReceived(notification) => {
                        let replaces_id = notification.replaces_id.unwrap();
                        {
                            let mut notification_data = notification_data_cloned.lock().await;
                            if let Some(old_notif) = notification_data
                                .insert(notification.id, NotificationData::new(&notification))
                            {
                                if let Some(handle) = old_notif.timeout_handle {
                                    handle.abort();
                                }
                            }
                        }
                        to_windowing_tx_cloned
                            .send(MessageToWindowing::ReplaceNotification(
                                replaces_id,
                                notification,
                            ))
                            .await?;
                    }
                    MessageFromDBus::CloseNotification(id) => {
                        let mut notification_data = notification_data_cloned.lock().await;
                        if let Some(n) = notification_data.get_mut(&id) {
                            to_windowing_tx_cloned
                                .send(MessageToWindowing::CloseNotification(id))
                                .await?;
                            n.close_reason.replace(CloseReason::CloseNotificationCalled);
                        }
                    }
                }
            }
            Ok(())
        },
        "Error in message handler from DBus",
    ));

    let notification_data_cloned = Arc::clone(&notification_data);
    let to_dbus_tx_cloned = to_dbus_tx.clone();
    let to_windowing_tx_cloned = to_windowing_tx.clone();
    let from_windowing_handler = task::spawn(crate::util::log_future_result(
        async move {
            while let Some(msg) = from_windowing_rx.recv().await {
                match msg {
                    MessageFromWindowing::NotificationClosed(id) => {
                        let reason = {
                            let mut notification_data = notification_data_cloned.lock().await;
                            notification_data
                                .remove(&id)
                                .and_then(|n| n.close_reason)
                                // if the windowing sends a NotificationClosed and no reason has
                                // been set yet, assume the user dismissed the notification
                                .unwrap_or(CloseReason::Dismissed)
                        };
                        log::debug!("Closing ID {} for reason {:?}", id, reason);
                        to_dbus_tx_cloned
                            .send(MessageToDBus::NotificationClosed(id, reason))
                            .await?;
                    }
                    MessageFromWindowing::StartNotificationTimeout(id) => {
                        let mut notification_data = notification_data_cloned.lock().await;
                        if let Some(n) = notification_data.get_mut(&id) {
                            if let Some(duration) = n.duration {
                                log::debug!("Starting notification expire task for ID {}", id);
                                let to_windowing_tx_cloned = to_windowing_tx_cloned.clone();
                                let notification_data_cloned =
                                    Arc::clone(&notification_data_cloned);
                                n.timeout_handle.replace(task::spawn(
                                    crate::util::log_future_result(
                                        async move {
                                            time::sleep(duration).await;
                                            let mut notification_data =
                                                notification_data_cloned.lock().await;
                                            to_windowing_tx_cloned
                                                .send(MessageToWindowing::CloseNotification(id))
                                                .await?;
                                            notification_data.get_mut(&id).map(|n| {
                                                n.close_reason.replace(CloseReason::Expired)
                                            });
                                            Ok(())
                                        },
                                        "Error trying to close notification window",
                                    ),
                                ));
                            }
                        }
                    }
                    MessageFromWindowing::InhibitNotificationTimeout(id) => {
                        let mut notification_data = notification_data_cloned.lock().await;
                        if let Some(n) = notification_data.get_mut(&id) {
                            if let Some(handle) = n.timeout_handle.take() {
                                log::debug!("Aborting notification expire task for ID {}", id);
                                handle.abort();
                            }
                        }
                    }
                    MessageFromWindowing::ActionInvoked(id, action_key) => {
                        to_dbus_tx_cloned
                            .send(MessageToDBus::ActionInvoked(id, action_key))
                            .await?;
                    }
                }
            }
            Ok(())
        },
        "Error in message handler from DBus",
    ));

    let result = tokio::join!(from_dbus_handler, from_windowing_handler);
    if let Err(e) = result.0 {
        log::error!("{}", e);
    }
    if let Err(e) = result.1 {
        log::error!("{}", e);
    }
}
