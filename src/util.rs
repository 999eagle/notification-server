use crate::messages::{MessageToDBus, MessageToWindowing};
use std::future::Future;
use tokio::signal;

#[derive(Debug, Copy, Clone)]
pub enum ReceivedSignal {
    CtrlC,
}

pub async fn run_signal_handler(
    to_dbus_tx: tokio::sync::mpsc::Sender<MessageToDBus>,
    to_windowing_tx: tokio::sync::mpsc::Sender<MessageToWindowing>,
) -> anyhow::Result<()> {
    let mut ctrl_c = signal::unix::signal(signal::unix::SignalKind::interrupt())?;

    loop {
        let signal = tokio::select! {
            result = ctrl_c.recv() => {
                result.map(|_| ReceivedSignal::CtrlC)
            }
        };

        match signal {
            Some(ReceivedSignal::CtrlC) => {
                log::info!("Received interrupt signal, exiting");
                to_dbus_tx.send(MessageToDBus::Exit).await?;
                to_windowing_tx.send(MessageToWindowing::Exit).await?;
                break;
            }
            _ => {}
        }
    }

    Ok(())
}

pub async fn log_future_result<F: Future<Output = anyhow::Result<()>>>(fut: F, context: &str) {
    if let Err(e) = fut.await {
        log::error!("{}: {}", context, e);
    }
}
