use crate::config::Config;
use crate::messages::{MessageFromWindowing, MessageToWindowing};
use crate::notification::{Notification, Urgency};
use gdk::prelude::*;
use glib::{MainContext, MainLoop};
use gtk::prelude::*;
use std::collections::HashMap;
use std::sync::Arc;
use tokio::sync::{mpsc, RwLock};

const NOTIFICATION_WINDOW_WIDTH: i32 = 300;
const NOTIFICATION_WINDOW_WIDTH_WITH_IMAGE: i32 = 400;
const NOTIFICATION_WINDOW_SPACING: i32 = 10;
const IMAGE_SIZE: i32 = 100;

const DEFAULT_CSS: &str = include_str!("style.css");

pub fn run_window_loop(
    config: Arc<Config>,
    mut rx: mpsc::Receiver<MessageToWindowing>,
    tx: mpsc::Sender<MessageFromWindowing>,
) -> anyhow::Result<()> {
    gtk::init()?;

    let context = MainContext::default();
    if !context.acquire() {
        log::error!("Failed to acquire main context!");
        return Err(anyhow::anyhow!("Failed to acquire main context!"));
    }

    if let Some(screen) = gdk::Screen::get_default() {
        let provider = gtk::CssProvider::new();
        provider.load_from_data(DEFAULT_CSS.as_bytes())?;
        gtk::StyleContext::add_provider_for_screen(
            &screen,
            &provider,
            gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
        );
        if let Some(style_file) = &config.style_file {
            log::debug!("Loading custom user styles");
            let provider = gtk::CssProvider::new();
            provider.load_from_path(style_file.as_os_str().to_string_lossy().as_ref())?;
            gtk::StyleContext::add_provider_for_screen(
                &screen,
                &provider,
                gtk::STYLE_PROVIDER_PRIORITY_USER,
            );
        }
    } else {
        log::warn!("Failed to get default screen!");
    }

    let main_loop = MainLoop::new(Some(&context), false);

    let windowing_data = Arc::new(RwLock::new(WindowingData {
        window_map: HashMap::new(),
        tx,
        context: context.clone(),
        total_window_height: 0,
    }));

    let main_loop_cloned = main_loop.clone();
    context.spawn_local(crate::util::log_future_result(
        async move {
            while let Some(msg) = rx.recv().await {
                match msg {
                    MessageToWindowing::Exit => {
                        main_loop_cloned.quit();
                    }
                    MessageToWindowing::DisplayNotification(notification) => {
                        let id = notification.id;
                        let window = NotificationWindow::new(notification, &windowing_data).await;
                        let mut windowing_data = windowing_data.write().await;
                        windowing_data.add_window(id, window);
                    }
                    MessageToWindowing::ReplaceNotification(id, notification) => {
                        let new_id = notification.id;
                        let existing_notification = {
                            let mut windowing_data_write = windowing_data.write().await;
                            windowing_data_write.window_map.remove(&id)
                        };
                        let window = if let Some(mut existing_notification) = existing_notification
                        {
                            existing_notification
                                .replace_with(notification, &windowing_data)
                                .await;
                            existing_notification
                        } else {
                            NotificationWindow::new(notification, &windowing_data).await
                        };
                        let mut windowing_data_write = windowing_data.write().await;
                        windowing_data_write.add_window(new_id, window);
                    }
                    MessageToWindowing::CloseNotification(notif_id) => {
                        let windowing_data = windowing_data.read().await;
                        windowing_data.close_window(notif_id);
                    }
                }
            }

            Ok(())
        },
        "Error in windowing message loop",
    ));

    main_loop.run();

    log::debug!("Windowing loop exited");

    Ok(())
}

struct WindowingData {
    pub window_map: HashMap<u32, NotificationWindow>,
    pub tx: mpsc::Sender<MessageFromWindowing>,
    pub context: MainContext,
    pub total_window_height: i32,
}

impl WindowingData {
    pub fn add_window(&mut self, id: u32, window: NotificationWindow) {
        self.window_map.insert(id, window);
        self.update_total_window_height();
    }

    pub fn close_window(&self, id: u32) {
        if let Some(window) = self.window_map.get(&id) {
            window.widgets.window.close();
        }
    }

    pub async fn handle_window_destroy(
        &mut self,
        id: u32,
        window_pos: (i32, i32),
    ) -> anyhow::Result<()> {
        self.window_map.remove(&id);
        self.tx
            .send(MessageFromWindowing::NotificationClosed(id))
            .await?;
        let height = self.total_window_height;
        self.update_total_window_height();
        let diff = height - self.total_window_height;
        self.update_window_positions(diff, window_pos.1);

        Ok(())
    }

    fn update_total_window_height(&mut self) {
        let mut top = i32::MAX;
        let mut bottom = i32::MIN;
        for window in self.window_map.values() {
            let window = &window.widgets.window;
            let window_top = window.get_position().1;
            let window_bottom = window_top + window.get_size().1 + NOTIFICATION_WINDOW_SPACING;
            top = top.min(window_top);
            bottom = bottom.max(window_bottom);
        }

        let height = if top == i32::MAX && bottom == i32::MIN {
            0
        } else {
            bottom - top
        };

        self.total_window_height = height;
    }

    fn update_window_positions(&mut self, diff: i32, window_top: i32) {
        for window in self.window_map.values() {
            let window = &window.widgets.window;
            let pos = window.get_position();
            if pos.1 > window_top {
                window.move_(pos.0, pos.1 - diff);
            }
        }
    }
}

struct NotificationWindow {
    pub notification_id: Arc<RwLock<u32>>,
    pub widgets: NotificationWindowWidgets,
    pub window_width: i32,
    pub window_height: i32,
}

#[derive(Clone)]
struct NotificationWindowWidgets {
    window: gtk::Window,
    overlay: gtk::Overlay,
    overlay_bg: gtk::Label,
    content: Option<gtk::Widget>,
}

impl NotificationWindow {
    async fn new(notification: Notification, windowing_data: &Arc<RwLock<WindowingData>>) -> Self {
        let notification_id = Arc::new(RwLock::new(notification.id));
        let widgets = Self::create_window(windowing_data, &notification_id).await;

        let mut window = Self {
            notification_id,
            widgets,
            window_width: 0,
            window_height: 0,
        };
        window.replace_with(notification, windowing_data).await;

        window
    }

    pub async fn replace_with(
        &mut self,
        notification: Notification,
        windowing_data: &Arc<RwLock<WindowingData>>,
    ) {
        {
            let mut notification_id = self.notification_id.write().await;
            *notification_id = notification.id;
        }

        let widgets = &mut self.widgets;

        // update window style
        let style_context = widgets.window.get_style_context();
        style_context.remove_class("urgency-critical");
        style_context.remove_class("urgency-normal");
        style_context.remove_class("urgency-low");
        match notification.urgency {
            Some(Urgency::Critical) => style_context.add_class("urgency-critical"),
            Some(Urgency::Low) => style_context.add_class("urgency-low"),
            Some(Urgency::Normal) | None => style_context.add_class("urgency-normal"),
        }

        // update window content
        let (content, window_width) =
            Self::build_content(&notification, windowing_data, &self.notification_id);
        self.window_width = window_width;

        let content_hmargins = content.get_margin_start() + content.get_margin_end();
        widgets.window.set_property_width_request(window_width);
        content.set_property_width_request(window_width - content_hmargins);

        // calculate height
        content.show_all();
        let height = content.get_preferred_height_for_width(window_width).0;
        widgets.overlay_bg.set_property_height_request(height);
        widgets.overlay.add_overlay(&content);
        if let Some(old_content) = widgets.content.replace(content) {
            widgets.overlay.remove(&old_content);
        }

        let height_diff = height - self.window_height;
        self.window_height = height;

        // show window
        widgets.window.show_all();

        if height_diff - height == 0 {
            // position window
            let current_stack_height = {
                let windowing_data = windowing_data.read().await;
                windowing_data.total_window_height
            };

            if let Some(primary_monitor) =
                gdk::Display::get_default().and_then(|display| display.get_primary_monitor())
            {
                let geom = primary_monitor.get_geometry();
                widgets.window.move_(
                    geom.x + geom.width - self.window_width - 10,
                    geom.y + 42 + current_stack_height,
                );
            } else {
                log::warn!("Failed to find primary monitor!")
            }
        } else {
            // re-position other windows
            let mut windowing_data = windowing_data.write().await;
            windowing_data.update_window_positions(-height_diff, widgets.window.get_position().1);
        }

        // start expiry timer
        {
            let windowing_data = windowing_data.read().await;
            if let Err(e) = windowing_data
                .tx
                .send(MessageFromWindowing::StartNotificationTimeout(
                    notification.id,
                ))
                .await
            {
                log::error!(
                    "Failed to start notification timeout for ID {}: {}",
                    notification.id,
                    e
                );
            }
        }
    }

    async fn create_window(
        windowing_data: &Arc<RwLock<WindowingData>>,
        notification_id: &Arc<RwLock<u32>>,
    ) -> NotificationWindowWidgets {
        log::debug!(
            "Building new window for notification with ID {}",
            *notification_id.read().await
        );
        let window = gtk::WindowBuilder::new()
            .type_(gtk::WindowType::Popup)
            .type_hint(gdk::WindowTypeHint::Notification)
            .hexpand(false)
            .vexpand(true)
            .resizable(false)
            .decorated(false)
            .visible(false)
            .role("notification")
            .can_focus(false)
            .build();
        window.get_style_context().add_class("notification");
        if let Some(rgba_visual) =
            gdk::Screen::get_default().and_then(|screen| screen.get_rgba_visual())
        {
            window.set_visual(Some(&rgba_visual));
        } else {
            log::warn!("Failed to get RGBA visual!");
        }

        let windowing_data_cloned = Arc::clone(windowing_data);
        let notification_id_cloned = Arc::clone(notification_id);
        window.connect_delete_event(move |window, _event| {
            let window_pos = window.get_position();
            let context = MainContext::default();
            let windowing_data_cloned = Arc::clone(&windowing_data_cloned);
            let notification_id_cloned = Arc::clone(&notification_id_cloned);
            context.spawn_local(crate::util::log_future_result(
                async move {
                    let notif_id = {
                        let notif_id = notification_id_cloned.read().await;
                        *notif_id
                    };
                    log::debug!("Closing notification window for ID {}", notif_id);
                    let mut windowing_data = windowing_data_cloned.write().await;
                    windowing_data
                        .handle_window_destroy(notif_id, window_pos)
                        .await?;
                    Ok(())
                },
                "Error in window destroy handler",
            ));

            gtk::Inhibit(false)
        });

        let windowing_data_cloned = Arc::clone(windowing_data);
        let notification_id_cloned = Arc::clone(notification_id);
        window.connect_enter_notify_event(move |_window, event| {
            let notif_type = event.get_detail();
            if notif_type == gdk::NotifyType::Inferior {
                return gtk::Inhibit(false);
            }
            let context = MainContext::default();
            let windowing_data_cloned = Arc::clone(&windowing_data_cloned);
            let notification_id_cloned = Arc::clone(&notification_id_cloned);
            context.spawn_local(crate::util::log_future_result(
                async move {
                    let notif_id = {
                        let notif_id = notification_id_cloned.read().await;
                        *notif_id
                    };
                    let windowing_data = windowing_data_cloned.read().await;
                    windowing_data
                        .tx
                        .send(MessageFromWindowing::InhibitNotificationTimeout(notif_id))
                        .await?;
                    Ok(())
                },
                "Error cancelling notification window timeout",
            ));
            gtk::Inhibit(true)
        });

        let windowing_data_cloned = Arc::clone(windowing_data);
        let notification_id_cloned = Arc::clone(notification_id);
        window.connect_leave_notify_event(move |_window, event| {
            let notif_type = event.get_detail();
            if notif_type == gdk::NotifyType::Inferior {
                return gtk::Inhibit(false);
            }
            let context = MainContext::default();
            let windowing_data_cloned = Arc::clone(&windowing_data_cloned);
            let notification_id_cloned = Arc::clone(&notification_id_cloned);
            context.spawn_local(crate::util::log_future_result(
                async move {
                    let notif_id = {
                        let notif_id = notification_id_cloned.read().await;
                        *notif_id
                    };
                    let windowing_data = windowing_data_cloned.read().await;
                    windowing_data
                        .tx
                        .send(MessageFromWindowing::StartNotificationTimeout(notif_id))
                        .await?;
                    Ok(())
                },
                "Error resetting notification window timeout",
            ));
            gtk::Inhibit(true)
        });

        window.connect_button_release_event(move |window, event| match event.get_button() {
            gdk::BUTTON_MIDDLE => {
                window.close();
                gtk::Inhibit(true)
            }
            _ => gtk::Inhibit(false),
        });

        log::trace!("Window created and events set up");

        let overlay = gtk::Overlay::new();
        overlay.set_vexpand(true);
        overlay.set_can_focus(false);
        let overlay_bg = gtk::Label::new(None);
        overlay.add(&overlay_bg);
        overlay_bg.set_can_focus(false);
        // overlay.add_overlay(&content);
        // let height = content.get_preferred_height_for_width(target_width).0;
        // overlay_child.set_property_height_request(height);
        window.add(&overlay);

        NotificationWindowWidgets {
            window,
            overlay,
            overlay_bg,
            content: None,
        }
    }

    fn build_content(
        notification: &Notification,
        windowing_data: &Arc<RwLock<WindowingData>>,
        notification_id: &Arc<RwLock<u32>>,
    ) -> (gtk::Widget, i32) {
        log::debug!("Building content for notification: {:?}", notification);
        let mut target_width = NOTIFICATION_WINDOW_WIDTH;

        let notification_box = gtk::Box::new(gtk::Orientation::Vertical, 0);
        notification_box.set_margin_start(15);
        notification_box.set_margin_end(15);
        notification_box.set_margin_top(10);
        notification_box.set_margin_bottom(10);
        notification_box.set_vexpand(true);
        notification_box.set_can_focus(false);

        let body_box = gtk::Box::new(gtk::Orientation::Horizontal, 15);
        notification_box.pack_start(&body_box, true, true, 0);
        body_box.set_vexpand(true);
        body_box.set_can_focus(false);
        body_box.set_margin_bottom(10);

        if let Some(image) = &notification.image {
            let notification_image = image.make_gtk_image(Some(IMAGE_SIZE));
            body_box.pack_start(&notification_image, false, false, 0);
            notification_image.set_valign(gtk::Align::Start);
            notification_image.set_can_focus(false);
            target_width = NOTIFICATION_WINDOW_WIDTH_WITH_IMAGE;
        }

        let content_box = gtk::Box::new(gtk::Orientation::Vertical, 0);
        body_box.pack_end(&content_box, true, true, 0);
        content_box.set_vexpand(true);
        content_box.set_valign(gtk::Align::Fill);
        content_box.set_can_focus(false);
        let summary_label = gtk::Label::new(Some(&notification.summary));
        content_box.pack_start(&summary_label, false, false, 0);
        summary_label
            .get_style_context()
            .add_class("notification-summary");
        summary_label.set_line_wrap(true);
        summary_label.set_line_wrap_mode(pango::WrapMode::WordChar);
        summary_label.set_halign(gtk::Align::Fill);
        summary_label.set_xalign(0.0);
        summary_label.set_can_focus(false);
        let body_label = gtk::Label::new(Some(&notification.body));
        content_box.pack_start(&body_label, false, false, 0);
        body_label
            .get_style_context()
            .add_class("notification-body");
        body_label.set_line_wrap(true);
        body_label.set_halign(gtk::Align::Fill);
        body_label.set_xalign(0.0);
        body_label.set_line_wrap_mode(pango::WrapMode::WordChar);
        body_label.set_can_focus(false);
        body_label.set_use_markup(true);

        let app_desc_box = gtk::Box::new(gtk::Orientation::Horizontal, 0);
        notification_box.pack_end(&app_desc_box, true, true, 0);
        app_desc_box.set_vexpand(true);
        app_desc_box.set_halign(gtk::Align::End);
        app_desc_box.set_valign(gtk::Align::End);
        app_desc_box.set_can_focus(false);
        let app_name = gtk::Label::new(Some(&notification.app_name));
        app_desc_box.pack_end(&app_name, false, false, 0);
        app_name
            .get_style_context()
            .add_class("notification-app-name");
        app_name.set_valign(gtk::Align::Center);
        app_name.set_can_focus(false);
        if let Some(icon) = &notification.icon {
            let app_icon = icon.make_gtk_image(None);
            app_desc_box.pack_end(&app_icon, false, false, 10);
            app_icon.set_can_focus(false);
        }

        if !notification.actions.is_empty() {
            let action_box = gtk::FlowBox::new();
            notification_box.pack_end(&action_box, true, true, 0);
            action_box.set_halign(gtk::Align::Fill);
            action_box.set_can_focus(false);
            action_box.set_margin_bottom(10);
            action_box.set_selection_mode(gtk::SelectionMode::None);

            for action in &notification.actions {
                let action_btn = gtk::Button::new();
                action_box.add(&action_btn);
                action_btn.set_label(&action.text);
                let key = action.key.clone();
                let windowing_data_cloned = Arc::clone(windowing_data);
                let notification_id_cloned = Arc::clone(notification_id);
                action_btn.connect_clicked(move |_btn| {
                    let context = MainContext::default();
                    let windowing_data_cloned = Arc::clone(&windowing_data_cloned);
                    let notification_id_cloned = Arc::clone(&notification_id_cloned);
                    let key = key.clone();
                    context.spawn_local(crate::util::log_future_result(
                        async move {
                            let notif_id = {
                                let notif_id = notification_id_cloned.read().await;
                                *notif_id
                            };
                            let windowing_data = windowing_data_cloned.read().await;
                            windowing_data
                                .tx
                                .send(MessageFromWindowing::ActionInvoked(notif_id, key))
                                .await?;
                            Ok(())
                        },
                        "Error resetting notification window timeout",
                    ));
                });
            }
        }

        (notification_box.upcast(), target_width)
    }
}
