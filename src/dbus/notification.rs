use crate::{messages::MessageFromDBus, notification::Notification};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fmt;
use zbus::{
    dbus_interface,
    zvariant::{self, Type},
};

pub struct NotificationServer {
    tx: super::MessageSender,
    info: ServerInformation,
    current_notification_id: u32,
}

#[derive(Clone)]
pub struct DBusNotification<'a> {
    pub app_name: &'a str,
    pub replaces_id: u32,
    pub app_icon: &'a str,
    pub summary: &'a str,
    pub body: &'a str,
    pub actions: Vec<&'a str>,
    pub hints: HashMap<&'a str, zvariant::Value<'a>>,
    pub expire_timeout: i32,
}

impl NotificationServer {
    pub fn new(tx: super::MessageSender, info: ServerInformation) -> Self {
        Self {
            tx,
            info,
            current_notification_id: 1,
        }
    }

    fn new_notification_id(&mut self) -> u32 {
        let id = self.current_notification_id;
        self.current_notification_id += 1;
        id
    }

    async fn send_message(&self, msg: MessageFromDBus) {
        let tx = self.tx.clone();
        match tx.send(msg).await {
            Ok(()) => {}
            Err(e) => log::error!("Failed to send message {:?}: {}", e.0, e),
        }
    }

    pub async fn emit_notification_closed(
        &self,
        ctx: &zbus::SignalContext<'_>,
        id: u32,
        reason: CloseReason,
    ) -> zbus::Result<()> {
        log::trace!(
            "Emitting notification closed signal for ID {} because: {:?}",
            id,
            reason
        );
        Self::notification_closed(ctx, id, reason).await
    }

    pub async fn emit_action_invoked(
        &self,
        ctx: &zbus::SignalContext<'_>,
        id: u32,
        action_key: &str,
    ) -> zbus::Result<()> {
        log::trace!(
            "Emitting action invoked signal for ID {} and action {:?}",
            id,
            action_key
        );
        Self::action_invoked(ctx, id, action_key).await
    }
}

#[allow(clippy::too_many_arguments)]
#[dbus_interface(name = "org.freedesktop.Notifications")]
impl NotificationServer {
    /// CloseNotification method
    async fn close_notification(&mut self, id: u32) -> zbus::fdo::Result<()> {
        log::trace!("Got request to close notification {}", id);
        self.send_message(MessageFromDBus::CloseNotification(id))
            .await;
        Ok(())
    }

    /// GetCapabilities method
    async fn get_capabilities(&self) -> zbus::fdo::Result<Vec<String>> {
        log::trace!("Got request for server capabilities");
        Ok(vec![
            "body".into(),
            "body-markup".into(),
            "body-hyperlinks".into(),
            "icon-static".into(),
            "actions".into(),
        ])
    }

    /// GetServerInformation method
    async fn get_server_information(&self) -> zbus::fdo::Result<ServerInformation> {
        log::trace!("Got request for server information");
        Ok(self.info.clone())
    }

    /// Notify method
    async fn notify(
        &mut self,
        app_name: &str,
        replaces_id: u32,
        app_icon: &str,
        summary: &str,
        body: &str,
        actions: Vec<&str>,
        hints: HashMap<&str, zvariant::Value<'_>>,
        expire_timeout: i32,
    ) -> zbus::fdo::Result<u32> {
        let notif = DBusNotification {
            app_name,
            replaces_id,
            app_icon,
            summary,
            body,
            actions,
            hints,
            expire_timeout,
        };
        log::debug!("Received notification from DBus: {:?}", notif);
        let notification_id = self.new_notification_id();
        log::trace!("Assigned ID {} to notification", notification_id);
        let notif = Notification::new(notification_id, notif);
        self.send_message(MessageFromDBus::NotificationReceived(notif))
            .await;

        Ok(notification_id)
    }

    /// ActionInvoked signal
    #[dbus_interface(signal)]
    async fn action_invoked(
        ctx: &zbus::SignalContext<'_>,
        id: u32,
        action_key: &str,
    ) -> zbus::Result<()>;

    /// NotificationClosed signal
    #[dbus_interface(signal)]
    async fn notification_closed(
        ctx: &zbus::SignalContext<'_>,
        id: u32,
        reason: CloseReason,
    ) -> zbus::Result<()>;
}

#[derive(Serialize, Deserialize, Type, Clone)]
pub struct ServerInformation {
    pub name: String,
    pub vendor: String,
    pub version: String,
    pub spec_version: String,
}

#[derive(Serialize, Deserialize, Type, Copy, Clone, Debug)]
#[repr(u32)]
pub enum CloseReason {
    Expired = 1,
    Dismissed = 2,
    CloseNotificationCalled = 3,
    Undefined = 4,
}

impl fmt::Debug for DBusNotification<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("DBusNotification")
            .field("app_name", &self.app_name)
            .field("replaces_id", &self.replaces_id)
            .field("app_icon", &self.app_icon)
            .field("summary", &self.summary)
            .field("body", &self.body)
            .field("actions", &self.actions)
            .field("hints", &HintsDebugHelper { hints: &self.hints })
            .field("expire_timeout", &self.expire_timeout)
            .finish()
    }
}

struct HintsDebugHelper<'h, 'a> {
    hints: &'h HashMap<&'a str, zvariant::Value<'a>>,
}

impl fmt::Debug for HintsDebugHelper<'_, '_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut hints = f.debug_map();
        for entry in self.hints {
            match *entry.0 {
                "image-data" | "image_data" | "icon_data" => match entry.1 {
                    zvariant::Value::Structure(structure) => {
                        hints.entry(
                            entry.0,
                            &format_args!(
                                "Structure( \"signature\": \"{}\" )",
                                structure.full_signature().as_str()
                            ),
                        );
                    }
                    _ => {
                        hints.entry(entry.0, entry.1);
                    }
                },
                _ => {
                    hints.entry(entry.0, entry.1);
                }
            }
        }
        hints.finish()
    }
}
