use crate::dbus::CloseReason;
use crate::notification::Notification;

#[derive(Debug)]
pub enum MessageFromDBus {
    NotificationReceived(Notification),
    CloseNotification(u32),
}

#[derive(Debug)]
pub enum MessageToDBus {
    NotificationClosed(u32, CloseReason),
    ActionInvoked(u32, String),
    Exit,
}

#[derive(Debug)]
pub enum MessageFromWindowing {
    NotificationClosed(u32),
    StartNotificationTimeout(u32),
    InhibitNotificationTimeout(u32),
    ActionInvoked(u32, String),
}

#[derive(Debug)]
pub enum MessageToWindowing {
    Exit,
    DisplayNotification(Notification),
    ReplaceNotification(u32, Notification),
    CloseNotification(u32),
}
