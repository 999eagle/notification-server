use serde::Deserialize;
use std::collections::HashMap;
use std::convert::TryInto;
use std::fmt;
use std::time::Duration;

const IMAGE_DATA_DBUS_SIGNATURE: &str = "(iiibiiay)";
const ICON_CHECKS: &[linicon_theme::Check] = &[
    linicon_theme::Check::GSettings,
    linicon_theme::Check::GTK3,
    linicon_theme::Check::GTK2,
    linicon_theme::Check::KDEGlobals,
    linicon_theme::Check::ThemeConf,
];

#[derive(Debug, Clone)]
pub struct Notification {
    pub id: u32,
    pub replaces_id: Option<u32>,
    pub summary: String,
    pub body: String,
    pub expiry: Expiry,
    pub app_name: String,
    pub icon: Option<Image>,
    pub image: Option<Image>,
    pub category: Option<Category>,
    pub urgency: Option<Urgency>,
    pub actions: Vec<Action>,
}

#[derive(Debug, Copy, Clone)]
pub enum Expiry {
    Never,
    Default,
    Duration(Duration),
}

#[derive(Debug, Clone)]
pub enum Image {
    Icon(String),
    File(String),
    Binary(ImageBinaryData),
}

#[derive(Clone)]
pub struct ImageBinaryData {
    pub has_alpha: bool,
    pub bits_per_sample: i32,
    pub width: i32,
    pub height: i32,
    pub rowstride: i32,
    pub bytes: glib::Bytes,
}

#[derive(Debug, Clone)]
pub struct Action {
    pub key: String,
    pub text: String,
}

#[derive(Debug, Clone, Deserialize)]
pub enum Category {
    #[serde(rename = "device")]
    Device,
    #[serde(rename = "device.added")]
    DeviceAdded,
    #[serde(rename = "device.error")]
    DeviceError,
    #[serde(rename = "device.removed")]
    DeviceRemoved,
    #[serde(rename = "email")]
    Email,
    #[serde(rename = "email.arrived")]
    EmailArrived,
    #[serde(rename = "email.bounced")]
    EmailBounced,
    #[serde(rename = "im")]
    Im,
    #[serde(rename = "im.error")]
    ImError,
    #[serde(rename = "im.received")]
    ImReceived,
    #[serde(rename = "network")]
    Network,
    #[serde(rename = "network.connected")]
    NetworkConnected,
    #[serde(rename = "network.disconnected")]
    NetworkDisconnected,
    #[serde(rename = "network.error")]
    NetworkError,
    #[serde(rename = "presence")]
    Presence,
    #[serde(rename = "presence.offline")]
    PresenceOffline,
    #[serde(rename = "presence.online")]
    PresenceOnline,
    #[serde(rename = "transfer")]
    Transfer,
    #[serde(rename = "transfer.complete")]
    TransferComplete,
    #[serde(rename = "transfer.error")]
    TransferError,
    Unknown(String),
}

#[enum_repr::EnumRepr(type = "u8")]
#[derive(Debug, Copy, Clone)]
pub enum Urgency {
    Low = 0,
    Normal = 1,
    Critical = 2,
}

impl Notification {
    pub fn new(id: u32, dbus_notification: crate::dbus::DBusNotification) -> Self {
        let replaces_id = if dbus_notification.replaces_id == 0 {
            None
        } else {
            Some(dbus_notification.replaces_id)
        };
        let expiry = match dbus_notification.expire_timeout {
            i if i < 0 => Expiry::Default,
            0 => Expiry::Never,
            i => Expiry::Duration(Duration::from_millis(i as _)),
        };

        let category =
            if let Some(zvariant::Value::Str(str)) = dbus_notification.hints.get("category") {
                Some(
                    serde_plain::from_str(str)
                        .ok()
                        .unwrap_or_else(|| Category::Unknown(str.to_string())),
                )
            } else {
                None
            };
        let urgency = if let Some(zvariant::Value::U8(val)) = dbus_notification.hints.get("urgency")
        {
            Urgency::from_repr(*val)
        } else {
            None
        };

        let actions = dbus_notification
            .actions
            .chunks_exact(2)
            .map(|a| Action {
                key: a[0].into(),
                text: a[1].into(),
            })
            .collect();

        let app_name_option = if !dbus_notification.app_name.is_empty() {
            Some(dbus_notification.app_name)
        } else {
            None
        };
        let app_icon_option = if !dbus_notification.app_icon.is_empty() {
            Some(dbus_notification.app_icon)
        } else {
            None
        };

        let icon = app_icon_option
            .map(|s| s.to_string())
            .and_then(check_icon)
            .or_else(|| {
                get_desktop_file_icon(&dbus_notification.hints, app_name_option)
                    .and_then(check_icon)
            })
            .or_else(|| app_name_option.map(|s| s.to_string()).and_then(check_icon))
            .or_else(|| {
                app_name_option
                    .map(|s| s.to_lowercase())
                    .and_then(check_icon)
            })
            .map(|icon| Image::from_text(&icon));

        let image = dbus_notification
            .hints
            .get("image-data")
            .or_else(|| dbus_notification.hints.get("image_data"))
            .or_else(|| dbus_notification.hints.get("image-path"))
            .or_else(|| dbus_notification.hints.get("image_path"))
            .or_else(|| dbus_notification.hints.get("icon_data"))
            .and_then(|data| match Image::from_image_data(data) {
                Ok(image) => Some(image),
                Err(e) => {
                    log::warn!(
                        "Error parsing notification image for notification {:?}: {}",
                        dbus_notification,
                        e
                    );
                    None
                }
            });

        Self {
            id,
            replaces_id,
            summary: dbus_notification.summary.into(),
            body: dbus_notification.body.into(),
            expiry,
            app_name: dbus_notification.app_name.into(),
            icon,
            image,
            category,
            urgency,
            actions,
        }
    }

    pub fn duration(&self) -> Option<Duration> {
        match self.expiry {
            Expiry::Duration(d) => Some(d),
            Expiry::Default => Some(std::time::Duration::from_millis(5000)),
            Expiry::Never => None,
        }
    }
}

fn get_desktop_file_icon(
    hints: &HashMap<&str, zvariant::Value>,
    app_name: Option<&str>,
) -> Option<String> {
    let desktop_file_name = hints
        .get("desktop-entry")
        .and_then(|v| {
            if let zvariant::Value::Str(s) = v {
                Some(s.as_str())
            } else {
                None
            }
        })
        .or(app_name);

    let desktop_file = desktop_file_name.and_then(|name| {
        let base_dirs = xdg::BaseDirectories::new().unwrap();
        base_dirs.find_data_file(format!("applications/{}.desktop", name))
    });

    desktop_file
        .and_then(|file| (freedesktop_entry_parser::parse_entry(file).ok()))
        .and_then(|file| {
            file.section("Desktop Entry")
                .attr("Icon")
                .map(|icon| icon.to_string())
        })
}

fn check_icon(icon: String) -> Option<String> {
    let exists = if icon.starts_with("file://") {
        // TODO: check whether file actually exists
        true
    } else {
        log::debug!("Checking whether icon name {} exists", icon);
        let theme = linicon_theme::get_icon_theme_order(ICON_CHECKS).unwrap();
        let mut result = linicon::lookup_icon(&icon).from_theme(theme);
        match result.next() {
            Some(Ok(_)) => true,
            Some(Err(e)) => {
                log::warn!("Error looking up icon with name {}: {}", icon, e);
                false
            }
            None => false,
        }
    };
    if exists {
        Some(icon)
    } else {
        None
    }
}

impl Image {
    fn from_icon(icon_name: &str) -> Self {
        Self::Icon(icon_name.to_string())
    }

    fn from_file(path: &str) -> Self {
        Self::File(path.to_string())
    }

    fn from_text(icon_text: &str) -> Self {
        if icon_text.starts_with("file://") {
            Self::from_file(icon_text)
        } else {
            Self::from_icon(icon_text)
        }
    }

    fn from_image_data(image_data: &zvariant::Value) -> anyhow::Result<Self> {
        let image_data = match image_data {
            zvariant::Value::Str(str) => return Ok(Self::from_text(str)),
            zvariant::Value::Structure(structure) => structure,
            _ => {
                return Err(anyhow::anyhow!("Expected string or structure"));
            }
        };
        if image_data.full_signature().as_str() != IMAGE_DATA_DBUS_SIGNATURE {
            return Err(anyhow::anyhow!(
                "Invalid image data structure signature: '{}' (expected '{}')",
                image_data.full_signature().as_str(),
                IMAGE_DATA_DBUS_SIGNATURE
            ));
        }
        let fields = image_data.fields();

        let width = *fields[0].downcast_ref::<i32>().unwrap();
        let height = *fields[1].downcast_ref::<i32>().unwrap();
        let rowstride = *fields[2].downcast_ref::<i32>().unwrap();
        let has_alpha = *fields[3].downcast_ref::<bool>().unwrap();
        let bits_per_sample = *fields[4].downcast_ref::<i32>().unwrap();
        let channels = *fields[5].downcast_ref::<i32>().unwrap();
        let data: zvariant::Array = (&fields[6]).try_into().unwrap();

        if (has_alpha && channels != 4)
            || (!has_alpha && channels != 3)
            || bits_per_sample != 8
            || (width * rowstride) as usize > data.len()
        {
            return Err(anyhow::anyhow!("Invalid image data"));
        }

        let data = data
            .iter()
            .map(|v| *v.downcast_ref::<u8>().unwrap())
            .collect::<Vec<_>>();
        let bytes = glib::Bytes::from_owned(data);

        Ok(Self::Binary(ImageBinaryData {
            has_alpha,
            bits_per_sample,
            width,
            height,
            rowstride,
            bytes,
        }))
    }

    pub fn make_gtk_image(&self, max_size: Option<i32>) -> gtk::Image {
        log::debug!(
            "Building GTK image widget for {:?} with size {:?}",
            self,
            max_size
        );
        match self {
            Self::Icon(icon) => gtk::Image::from_icon_name(Some(icon), gtk::IconSize::SmallToolbar),
            Self::File(path) => {
                let pixbuf = if let Some(size) = max_size {
                    gdk_pixbuf::Pixbuf::from_file_at_scale(&path, size, size, true)
                } else {
                    gdk_pixbuf::Pixbuf::from_file(&path)
                };
                if let Err(e) = &pixbuf {
                    log::warn!("Failed loading image from {:?}: {}", path, e);
                }
                gtk::Image::from_pixbuf(pixbuf.ok().as_ref())
            }
            Self::Binary(binary) => {
                let pixbuf = gdk_pixbuf::Pixbuf::from_bytes(
                    &binary.bytes,
                    gdk_pixbuf::Colorspace::Rgb,
                    binary.has_alpha,
                    binary.bits_per_sample,
                    binary.width,
                    binary.height,
                    binary.rowstride,
                );
                let pixbuf = if let Some(size) = max_size {
                    let scale_factor = (size as f32) / (binary.width.max(binary.height) as f32);
                    let new_width = (binary.width as f32 * scale_factor) as i32;
                    let new_height = (binary.height as f32 * scale_factor) as i32;
                    pixbuf.scale_simple(new_width, new_height, gdk_pixbuf::InterpType::Bilinear)
                } else {
                    Some(pixbuf)
                };
                gtk::Image::from_pixbuf(pixbuf.as_ref())
            }
        }
    }
}

impl fmt::Debug for ImageBinaryData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("ImageBinaryData")
            .field("has_alpha", &self.has_alpha)
            .field("bits_per_sample", &self.bits_per_sample)
            .field("width", &self.width)
            .field("height", &self.height)
            .field("rowstride", &self.rowstride)
            .field("bytes", &format_args!("{}", ".."))
            .finish() // TODO: replace this with .finish_non_exhaustive once stable
    }
}
