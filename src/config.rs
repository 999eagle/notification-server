use bin_common::{CrateSetup, CrateSetupBuilder};
use clap::{App, Arg, ArgMatches};
use std::path::PathBuf;
use version::version;

pub struct Config {
    pub crate_setup: CrateSetup,
    pub base_dirs: xdg::BaseDirectories,
    pub style_file: Option<PathBuf>,
}

pub fn get_crate_setup() -> CrateSetup {
    CrateSetupBuilder::new()
        .with_app_name("notification-server")
        .build()
        .expect("CrateSetup")
}

pub fn get_app<'c, 'a, 'b>(crate_setup: &'c CrateSetup) -> App<'a, 'b> {
    App::new(crate_setup.application_name().to_string())
        .version(version!())
        .about("A Linux notification server implemented in Rust.")
        .arg(
            Arg::with_name("v")
                .short("v")
                .long("verbose")
                .multiple(true)
                .help("Increase verbosity. May be specified multiple times."),
        )
        .arg(
            Arg::with_name("q")
                .short("q")
                .long("quiet")
                .multiple(true)
                .help("Decrease verbosity. May be specified multiple times."),
        )
}

impl Config {
    pub fn from_arg_matches(matches: &ArgMatches, crate_setup: CrateSetup) -> Self {
        let base_dirs = xdg::BaseDirectories::with_prefix("notification-server")
            .expect("Failed to get XDG base directories");
        let style_file = base_dirs.find_config_file("style.css");
        Self {
            crate_setup,
            base_dirs,
            style_file,
        }
    }
}
