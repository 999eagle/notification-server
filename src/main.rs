use std::sync::Arc;

mod config;
mod dbus;
mod management;
mod messages;
mod notification;
mod util;
mod windowing;

fn main() {
    let rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .unwrap();

    if let Err(e) = rt.block_on(main_result()) {
        log::error!("{}", e);
    }
}

async fn main_result() -> anyhow::Result<()> {
    let crate_setup = config::get_crate_setup();
    let matches = config::get_app(&crate_setup).get_matches();
    // default to 1 (INFO), allow increasing and decreasing
    let verbosity = 1 + matches.occurrences_of("v") as i8 - matches.occurrences_of("q") as i8;
    crate_setup
        .logging_setup()
        .with_verbosity(verbosity)
        .build()
        .expect("Failed to setup output!");
    let config = config::Config::from_arg_matches(&matches, crate_setup);
    let config = Arc::new(config);
    log::info!("{} starting", config.crate_setup.application_name());

    let (to_windowing_tx, to_windowing_rx) = tokio::sync::mpsc::channel(5);
    let (from_windowing_tx, from_windowing_rx) = tokio::sync::mpsc::channel(5);
    let config_cloned = Arc::clone(&config);
    let windowing_thread = std::thread::spawn(|| {
        windowing::run_window_loop(config_cloned, to_windowing_rx, from_windowing_tx)
    });

    let (from_dbus_tx, from_dbus_rx) = tokio::sync::mpsc::channel(5);
    let (to_dbus_tx, to_dbus_rx) = tokio::sync::mpsc::channel(5);
    let cloned_config = Arc::clone(&config);
    let local_tasks = tokio::task::LocalSet::new();
    let dbus_handle =
        local_tasks.run_until(dbus::run_dbus(cloned_config, from_dbus_tx, to_dbus_rx));

    let management_handle = tokio::task::spawn(management::run_message_handlers(
        from_dbus_rx,
        from_windowing_rx,
        to_dbus_tx.clone(),
        to_windowing_tx.clone(),
    ));

    let signal_handler_handle = tokio::task::spawn(util::run_signal_handler(
        to_dbus_tx.clone(),
        to_windowing_tx.clone(),
    ));

    let result = tokio::join!(dbus_handle, signal_handler_handle);
    result.0?;
    result.1??;

    if let Err(panic) = windowing_thread.join() {
        std::panic::resume_unwind(panic);
    }

    Ok(())
}
